package com.hrossi.explore

import android.app.Application
import com.hrossi.explore.di.networkModule
import com.hrossi.explore.di.repositoryModule
import com.hrossi.explore.di.viewModelModule
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ExploreApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@ExploreApplication)
            modules(listOf(viewModelModule, networkModule, repositoryModule))
        }

        RxJavaPlugins.setErrorHandler { }
    }

}