package com.hrossi.explore.di

import com.hrossi.explore.data.remote.repository.restaurant.RestaurantRepository
import com.hrossi.explore.data.remote.repository.restaurant.RestaurantRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    
    factory<RestaurantRepository> {
        RestaurantRepositoryImpl(
            get()
        )
    }

}