package com.hrossi.explore.di

import com.hrossi.explore.ui.explore.ExploreViewModel
import com.hrossi.explore.ui.about.AboutViewModel
import com.hrossi.explore.ui.restaurant.RestaurantDetailViewModel
import com.hrossi.explore.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { ExploreViewModel(get()) }
    viewModel { SearchViewModel(get()) }
    viewModel { AboutViewModel() }
    viewModel { RestaurantDetailViewModel(get()) }

}