package com.hrossi.explore.di

import com.hrossi.explore.BuildConfig
import com.hrossi.explore.data.remote.ApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single { providesHttpLoggingInterceptor() }
    single { providesHeaderInterceptor() }
    single { providesOkHttp(get(), get()) }
    single { providesRetrofit(get()) }
    single { providesApiService(get()) }

}

fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }
}

fun providesHeaderInterceptor(): Interceptor {
    return object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request().newBuilder()
            request.addHeader("user-key", "ec72f2ac8a160427218fa2d52c0bea30")
            return chain.proceed(request.build())
        }
    }
}

fun providesOkHttp(loggingInterceptor: HttpLoggingInterceptor, headerInterceptor: Interceptor): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .addInterceptor(headerInterceptor)
        .build()
}

fun providesRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://developers.zomato.com/api/")
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun providesApiService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}