package com.hrossi.explore

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.api.load
import coil.transform.RoundedCornersTransformation

@BindingAdapter("custom:visible")
fun visibility(view: View, visible: Boolean) {
    if (visible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("custom:load")
fun load(imageView: ImageView, load: String?) {
    imageView.load(load) {
        crossfade(500)
        transformations(RoundedCornersTransformation(8F))
    }
}