package com.hrossi.explore.ui.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import coil.api.load
import coil.transform.CircleCropTransformation
import com.hrossi.explore.R
import com.hrossi.explore.databinding.FragmentAboutBinding
import kotlinx.android.synthetic.main.fragment_about.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AboutFragment : Fragment() {

    private val notificationsViewModel: AboutViewModel by viewModel()

    private lateinit var fragmentAboutBinding: FragmentAboutBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentAboutBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false)
        return fragmentAboutBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        imageViewAvatar.load("https://media.licdn.com/dms/image/C5603AQHuwbvsEnd8Ag/profile-displayphoto-shrink_200_200/0?e=1574899200&v=beta&t=ixDsYSDVuyBjyW8iLz_OfClO0nuLI4sprghEwL4BLbA") {
            transformations(CircleCropTransformation())
        }
    }

    private fun setupListeners() {
        imageViewLinkedIn.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/hugo-h-rossi/"))
            startActivity(intent)
        }
        imageViewGithub.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.github.com/hrossi"))
            startActivity(intent)
        }
        imageViewEmail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:hugo.hrossi@gmail.com")
            startActivity(intent)
        }
    }

}
