package com.hrossi.explore.ui

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {

    private var mCompositeDisposable = CompositeDisposable()

    private fun getCompositeDisposable(): CompositeDisposable {
        if (mCompositeDisposable.isDisposed) {
            mCompositeDisposable = CompositeDisposable()
        }
        return mCompositeDisposable
    }

    private fun onStopDisposable() {
        getCompositeDisposable().clear()
    }

    protected fun Disposable.addDisposableBy() {
        getCompositeDisposable().add(this)
    }

    override fun onCleared() {
        super.onCleared()
        onStopDisposable()
    }

}