package com.hrossi.explore.ui.explore.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.hrossi.explore.R
import com.hrossi.explore.data.remote.model.Restaurant
import kotlinx.android.synthetic.main.list_item_explore.view.*

class ExploreFragmentViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    private val name = view.textViewName
    private val openings = view.textViewOpeningHours
    private val thumb = view.imageViewLogo
    private val price = view.textViewPrice

    fun format(restaurants: Restaurant) {
        name.text = restaurants.name.toLowerCase().capitalize()
        openings.text = restaurants.openings?.toLowerCase()
        price.text = restaurants.price
        thumb.load(restaurants.thumb) {
            error(R.drawable.ic_store_black_24dp)
            transformations(RoundedCornersTransformation(8F))
        }
    }

}