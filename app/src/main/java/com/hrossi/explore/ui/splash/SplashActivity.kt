package com.hrossi.explore.ui.splash

import androidx.appcompat.app.AppCompatActivity
import com.hrossi.explore.ui.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        MainActivity.createIntent(this).let {
            startActivity(it)
            finish()
        }
    }

}