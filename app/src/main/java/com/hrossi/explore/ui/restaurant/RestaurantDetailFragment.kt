package com.hrossi.explore.ui.restaurant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.hrossi.explore.R
import com.hrossi.explore.databinding.RestaurantDetailFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RestaurantDetailFragment : Fragment() {

    private val restaurantDetailViewModel: RestaurantDetailViewModel by viewModel()

    private lateinit var restaurantDetailFragmentBinding: RestaurantDetailFragmentBinding

    val args: RestaurantDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        restaurantDetailFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.restaurant_detail_fragment, container, false)
        return restaurantDetailFragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        restaurantDetailViewModel.load(args.restaurantId)
    }

    private fun bindViewModel() {
        restaurantDetailFragmentBinding.let {
            it.restaurantDetailsViewModelInView = restaurantDetailViewModel
            it.lifecycleOwner = this
        }
    }

}
