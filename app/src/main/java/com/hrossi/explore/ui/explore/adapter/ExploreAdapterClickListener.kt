package com.hrossi.explore.ui.explore.adapter

import com.hrossi.explore.data.remote.model.Restaurant

interface ExploreAdapterClickListener {

    fun onClick(restaurant: Restaurant)

}