package com.hrossi.explore.ui.explore.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.hrossi.explore.R
import com.hrossi.explore.data.remote.model.Restaurant
import com.hrossi.explore.databinding.ListItemExploreBinding

class ExploreFragmentAdapter(
    private var list: List<Restaurant>,
    private val listener: ExploreAdapterClickListener
) : RecyclerView.Adapter<ExploreFragmentViewHolder>() {

    private lateinit var listItemExploreBinding: ListItemExploreBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExploreFragmentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        listItemExploreBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_explore, parent, false)
        return ExploreFragmentViewHolder(listItemExploreBinding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ExploreFragmentViewHolder, position: Int) {
        val item = list[position]
        holder.format(item)
        holder.itemView.setOnClickListener { listener.onClick(item) }
    }

    fun setRestaurants(restaurants: List<Restaurant>) {
        list = restaurants
        notifyDataSetChanged()
    }

}