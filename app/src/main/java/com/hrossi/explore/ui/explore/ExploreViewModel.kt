package com.hrossi.explore.ui.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hrossi.explore.data.remote.model.Restaurant
import com.hrossi.explore.data.remote.repository.restaurant.RestaurantRepository
import com.hrossi.explore.ui.BaseViewModel

class ExploreViewModel(private val restaurantRepository: RestaurantRepository) : BaseViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _restaurants = MutableLiveData<List<Restaurant>>()
    val restaurants: LiveData<List<Restaurant>> = _restaurants

    init {
        getRestaurants()
    }

    fun onRefresh() {
        _restaurants.postValue(emptyList())
        getRestaurants()
    }

    private fun getRestaurants() {
        _loading.postValue(true)
        restaurantRepository.search()
            .doOnSuccess(_restaurants::postValue)
            .doFinally { _loading.postValue(false) }
            .subscribe()
            .addDisposableBy()
    }

}