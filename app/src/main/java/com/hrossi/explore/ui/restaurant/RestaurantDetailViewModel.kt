package com.hrossi.explore.ui.restaurant

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hrossi.explore.data.remote.model.Restaurant
import com.hrossi.explore.data.remote.repository.restaurant.RestaurantRepository
import com.hrossi.explore.ui.BaseViewModel

class RestaurantDetailViewModel(private val restaurantRepository: RestaurantRepository) : BaseViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _restaurant = MutableLiveData<Restaurant>()
    val restaurant: LiveData<Restaurant> = _restaurant

    fun load(id: String) {
        _loading.postValue(true)
        restaurantRepository.get(id)
            .doOnSuccess { _restaurant.postValue(it) }
            .doFinally { _loading.postValue(false) }
            .subscribe()
            .addDisposableBy()
    }

}