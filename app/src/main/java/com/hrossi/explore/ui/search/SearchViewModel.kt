package com.hrossi.explore.ui.search

import com.hrossi.explore.data.remote.repository.restaurant.RestaurantRepository
import com.hrossi.explore.ui.BaseViewModel

class SearchViewModel(private val repo : RestaurantRepository) : BaseViewModel()