package com.hrossi.explore.ui.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.hrossi.explore.R
import com.hrossi.explore.data.remote.model.Restaurant
import com.hrossi.explore.databinding.FragmentExploreBinding
import com.hrossi.explore.ui.explore.adapter.ExploreFragmentAdapter
import com.hrossi.explore.ui.explore.adapter.ExploreAdapterClickListener
import kotlinx.android.synthetic.main.fragment_explore.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ExploreFragment : Fragment() {

    private val exploreViewModel: ExploreViewModel by viewModel()

    private lateinit var fragmentExploreBinding: FragmentExploreBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentExploreBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore, container, false)
        return fragmentExploreBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        setupListeners()
        setupRecycler()
        setupObservable()
    }

    private fun setupObservable() {
        exploreViewModel.restaurants.observe(this, Observer {
            (fragmentExploreBinding.recyclerExplore.adapter as ExploreFragmentAdapter).setRestaurants(it)
        })
    }

    private fun setupRecycler() {
        fragmentExploreBinding.recyclerExplore.let {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.addItemDecoration(DividerItemDecoration(it.context, LinearLayoutManager.VERTICAL))
            it.adapter = ExploreFragmentAdapter(emptyList(), object : ExploreAdapterClickListener {
                override fun onClick(restaurant: Restaurant) {
                    val action = ExploreFragmentDirections.actionNavExploreToRestaurantDetailFragment(restaurant.id)
                    findNavController(this@ExploreFragment).navigate(action)
                }
            })
        }
    }

    private fun setupListeners() {
        swipeToRefreshExplore.setOnRefreshListener {
            exploreViewModel.onRefresh()
            swipeToRefreshExplore.isRefreshing = false
        }
    }

    private fun bindViewModel() {
        fragmentExploreBinding.let {
            it.exploreViewModelInView = exploreViewModel
            it.lifecycleOwner = this
        }
    }

}