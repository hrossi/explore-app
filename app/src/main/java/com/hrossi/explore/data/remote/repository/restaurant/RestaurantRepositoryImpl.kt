package com.hrossi.explore.data.remote.repository.restaurant

import com.hrossi.explore.data.mapper.RestaurantListMapper
import com.hrossi.explore.data.mapper.RestaurantMapper
import com.hrossi.explore.data.remote.ApiService
import com.hrossi.explore.data.remote.model.Restaurant
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RestaurantRepositoryImpl(private val service: ApiService) : RestaurantRepository {

    override fun search(): Single<List<Restaurant>> {
        return service.search()
            .map { RestaurantListMapper().transform(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun get(id: String): Single<Restaurant> {
        return service.get(id)
            .map { RestaurantMapper().transform(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
