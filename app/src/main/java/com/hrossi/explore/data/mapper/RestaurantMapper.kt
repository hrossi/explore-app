package com.hrossi.explore.data.mapper

import com.hrossi.explore.data.mapper.base.Mapper
import com.hrossi.explore.data.remote.dto.RestaurantDTO
import com.hrossi.explore.data.remote.model.Restaurant

class RestaurantMapper : Mapper<RestaurantDTO, Restaurant> {

    override fun transform(restaurant: RestaurantDTO): Restaurant {
        var price = ""
        for (i in 0 until restaurant.priceRange) {
            price += "$"
        }
        return Restaurant(
            restaurant.id,
            restaurant.name,
            restaurant.cuisines,
            restaurant.timings,
            restaurant.thumb,
            price,
            restaurant.location.address,
            restaurant.photos?.firstOrNull()?.photos?.url
        )
    }

}