package com.hrossi.explore.data.remote.model

data class Restaurant(

    val id: String,
    val name: String,
    val categories : String,
    val openings: String?,
    val thumb: String,
    val price: String,
    val address: String,
    val picture : String?

)