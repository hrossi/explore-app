package com.hrossi.explore.data.mapper.base

interface Mapper<in X, out Y> {

    fun transform(x: X): Y

}