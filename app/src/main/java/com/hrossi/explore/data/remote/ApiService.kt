package com.hrossi.explore.data.remote

import com.hrossi.explore.data.remote.dto.RestaurantDTO
import com.hrossi.explore.data.remote.dto.RestaurantListDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("v2.1/search")
    fun search(): Single<RestaurantListDTO>

    @GET("v2.1/restaurant")
    fun get(@Query("res_id") id: String): Single<RestaurantDTO>

}