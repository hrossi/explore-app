package com.hrossi.explore.data.mapper

import com.hrossi.explore.data.mapper.base.Mapper
import com.hrossi.explore.data.remote.dto.RestaurantListDTO
import com.hrossi.explore.data.remote.model.Restaurant

class RestaurantListMapper : Mapper<RestaurantListDTO, List<Restaurant>> {

    override fun transform(restaurants: RestaurantListDTO): List<Restaurant> {
        val out = mutableListOf<Restaurant>()

        restaurants.restaurants.forEach {
            var price = ""
            for (x in 0 until it.restaurant.priceRange) {
                price += "$"
            }
            out.add(
                Restaurant(
                    it.restaurant.id,
                    it.restaurant.name,
                    it.restaurant.cuisines,
                    it.restaurant.timings,
                    it.restaurant.thumb,
                    price,
                    it.restaurant.location.address,
                    it.restaurant.photos?.firstOrNull()?.photos?.url
                )
            )
        }

        return out
    }

}