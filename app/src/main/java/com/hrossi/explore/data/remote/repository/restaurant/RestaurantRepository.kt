package com.hrossi.explore.data.remote.repository.restaurant

import com.hrossi.explore.data.remote.model.Restaurant
import io.reactivex.Single

interface RestaurantRepository {

    fun search(): Single<List<Restaurant>>

    fun get(id: String): Single<Restaurant>

}