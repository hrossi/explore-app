package com.hrossi.explore.data.remote.dto

import com.google.gson.annotations.SerializedName

data class RestaurantListDTO(
    @SerializedName("restaurants")
    val restaurants: List<RestaurantsDTO>
)

data class RestaurantsDTO(
    @SerializedName("restaurant")
    val restaurant: RestaurantDTO
)

data class RestaurantDTO(
    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("cuisines")
    val cuisines: String,

    @SerializedName("timings")
    val timings: String,

    @SerializedName("thumb")
    val thumb: String,

    @SerializedName("price_range")
    val priceRange: Int,

    @SerializedName("location")
    val location: LocationDTO,

    @SerializedName("highlights")
    val highlights: List<String>,

    @SerializedName("average_cost_for_two")
    val averageCost: Float,

    @SerializedName("currency")
    val currency: String,

    @SerializedName("photos")
    val photos: List<PhotosDTO>?
)

data class LocationDTO(
    @SerializedName("address")
    val address: String
)

data class PhotosDTO(
    @SerializedName("photo")
    val photos: PhotoDTO
)

data class PhotoDTO(
    @SerializedName("url")
    val url: String
)